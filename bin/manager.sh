#!/bin/bash

kill_worker()
{
    if [ -e "./pids/$2_$1.pid" ]; then
        case $3 in
            'restart')
                kill -SIGHUP `cat ./pids/$2_$1.pid`
            ;;
            'stop')
                kill -SIGQUIT `cat ./pids/$2_$1.pid`
            ;;
        esac
        rm pids/$2_$1.pid;
        echo 'done';
    fi;
}

start_script()
{
    if [ -e "run_$1_$2.sh" ]; then
        ./run_$1_$2.sh
    fi;
}

case $1 in
    'updater' | 'weirdness')
        case $2 in
            'restart' | 'stop')
                kill_worker 'sources' $1 $2;
                ;;
            'start')
                ./run_$1.sh
                ;;
        esac
        ;;
    'all_workers')
        case $2 in
            'start')
                for WORKER in 1 2 3
                do
                    start_script "source$WORKER" 'normalizer';
                done
                ./run_weirdness.sh
                ./run_updater.sh
            ;;
            'stop')
                for WORKER in 1 2 3
                do
                    kill_worker "normalizer" "source$WORKER" 'stop';
                done
                kill_worker 'sources' 'weirdness' 'stop';
                kill_worker 'sources' 'updater' 'stop';
            ;;
        esac
    ;;
    *)
        case $3 in
            'restart' | 'stop')
                kill_worker $1 $2 $3;
                ;;
            'start')
                start_script $2 $1
                ;;
        esac
    ;;
esac
