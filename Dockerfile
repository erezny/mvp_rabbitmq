FROM ruby:2.2.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

RUN mkdir /mvp_rabbitmq
WORKDIR /mvp_rabbitmq

ADD ./ /mvp_rabbitmq
RUN bundle install

ENTRYPOINT ./manager.sh
CMD ["all_worker", "start"]
