require_relative '../shared/observer'
require "aws-sdk"
require 'dotenv'
Dotenv.load 

class Observer4 < Observer

   def initialize
     super('source4')
   end

  def generate_data  
    specific_field = {"photos" => [], "price" => rand(1000000)}
    2.times {specific_field["photos"] << {"url" => get_random_image_url()}} 
    super().merge(specific_field)
  end

  def get_random_image_url
    ["https://cdn-images-1.medium.com/fit/t/800/240/1*WaV5Kk-qbRxy6lD4lLVIKQ.jpeg",
    "https://cdn-images-1.medium.com/fit/t/800/240/1*MBZjmOkmAGjuF_s-YFKzXA.png",
    "https://cdn-images-1.medium.com/fit/t/800/240/1*QLwd3yvHdV4GDzWKloO-tg.jpeg",
    "https://cdn-images-1.medium.com/fit/t/800/240/1*QLwd3yvHdV4GDzWKloO-tg.jeg",
    "https://cdn-images-1.medium.com/max/343/1*Eh8vVETymU9TjojhCz14Kw.jpeg",
    "https://cdn-images-1.medium.com/fit/t/800/240/1*fiSY5_Yzn4qJg5v1cGhx7g.jpeg",
    "https://cdn-images-1.medium.com/max/175/1*idByuBwsn6o5n0XhbR-yFg.jpeg",
    "https://cdn-images-1.medium.com/max/400/0*YH7KvmBuAeZmqm1g.jpg"].sample
  end

  def run(count = 1000)
    super(count)
    s3 = Aws::S3::Resource.new(region: ENV['S3_REGION'])
    bucket = s3.bucket(ENV['S3_BUCKET'])
    bucket.clear!
  end

end
