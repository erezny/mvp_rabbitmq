require_relative '../shared/observer'

class Observer1 < Observer

   def initialize
     super('source1')
   end

  def generate_data  
    specific_field = {
        :o1_price   => rand(300..9999),
        :o1_surface => FFaker::NameRU.name,
    }

    super().merge(specific_field)
  end

end
