require_relative '../shared/processor'

Sneakers.logger.level = Logger::INFO

Sneakers.configure(:daemonize => true,
                   :threads => 1,
                   :prefetch => 10,
                   :heartbeat => 2,
                   :retry_max_times => 5,
                   :timeout_job_after => 1,
                   :retry_timeout => 1000,
                   :pid_path => 'pids/source2_normalizer.pid',
                   :log => 'logs/source2_normalizer.log',
                   :handler=> Sneakers::Handlers::Maxretry)

class Source2NormalizerWorker < Processor

  from_queue 'source2_observed_ads'

  def process(data)
    super(data)
  end

end

