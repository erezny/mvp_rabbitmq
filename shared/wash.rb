require 'hashie'

# A Wash is a `cleaned` Trash where undeclarated keys
# from a source hash are ignored
class Wash < Hashie::Trash
  def initialize_attributes(attributes)
    return unless attributes
    attributes_copy = attributes.dup.keep_if do |k, v|
      self.class.translations_hash.include?(k) || self.class.property?(k)
    end
    super attributes_copy
  end
end
