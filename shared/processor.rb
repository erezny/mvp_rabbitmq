require 'sneakers'
require 'sneakers/handlers/maxretry'
require 'json'

require_relative '../shared/mapper'


# generic processor class
class Processor
  include Sneakers::Worker

	def work(msg)
		begin
		 	data = JSON.parse(msg)
      emulate_exception
		 	process(data)
		rescue Exception => e
      Sneakers::logger.error("Normalization ERR! #{e}")
      # requeue!
      puts '-------'
      puts @error
      puts '------------'
    end
    ack!
	end

	def emulate_exception
    if rand(100) <= 5
      requeue!
      raise Exception.new("[Fake error] Nicolas! Warning! It's work!")
    end
  end

  def process(data)
    clean_data = Mapper.new(data)
    if clean_data
      if clean_data[:ads_uid] && clean_data[:price]
        Sneakers.publish( clean_data.to_json, :to_queue => 'normalized_ads_queue' )
      else
        Sneakers::logger.error("Normalization ERR! We have no required fields for '#{data}'")
      end
    else
      Sneakers::logger.error("Normalization ERR! We have no normalizers for '#{data[:source]}'")
    end
  end
end
