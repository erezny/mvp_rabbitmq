require 'bunny'
require 'json'
require 'ffaker'

class Observer

	def initialize(source_name)
	 	@source_name = source_name   
  end


  def run(count = 1000)
  	conn = Bunny.new
    conn.start

    channel = conn.create_channel
    exchange = channel.direct('')
 
    count.times do | i|
    		data = generate_data
    		exchange.publish(data.to_json, :routing_key => "#{@source_name}_observed_ads", :durable => true)
    end

    ## !!! you need to stop the connexion
    conn.stop
  end

  protected 

  def generate_data
      {
          :ads_uid    => rand(1000000),
          :house_uid  => rand(100),
          :date       => Time.now,       
          :source     => @source_name
      }
  end

end
